# Propostes references a Infrastructura de Telecomunicacions

## Consolidació d'un ecosistema d'infraestructura col·laborativa i oberta de tecnologies lliures

**Objectius**
  * Tendir cap a la universalització i sobirania de les xarxes de telecomunicacions.

**Proposta programàtica**
  * Proporcionar accés real a la ciutadania i a la societat en general a una oferta assequible i variada de serveis de telecomunicacions de la màxima qualitat, capacitat i neutrals.
  * Promoure ordenances municipals de desplegament de nova infraestructura com la proposada per Guifi.net.
  * Afavorir les xarxes comunals i obertes, incidint en la necessitat d'avaluació del impacte social i territorial dels desplegaments, tenint en compte els possibles casos d'ús.

**Experiències inspiradores**
  * Guifi.net: https://fundacio.guifi.net/ca_ES/
  * eXO.cat: https://exo.cat/lassociacio/
  * Ordenança municipal per al desplegament de xarxes d'accés a serveis de telecomunicacions de nova generació i en format universal: https://fundacio.guifi.net/web/content/2322?unique=cef4bebe39b45ba50ed5ebb5e2a63ecaf07e6cb4&download=true

**Males pràctiques**
  * Free Basic: https://info.internet.org/es/
  * Absorció d'Iberbanda per Telefónica, amb increment de preu: http://afreixinet.com/desconnectats-movistar-tanca-iberbanda/ https://www.santquirzesafaja.es/not%C3%ADcies/banda-ampla-rural-de-wimax-a-afr 

## Internet de les Coses
**Objectius**
  * Desplegament i consolidació d’una xarxa de dades d’Internet de les coses oberta, lliure, neutral creada col·lectivament des de baix.

**Proposta programàtica**
  * Promoure les iniciatives de xarxes internet dels objectes oberta, lliure i neutral.
  * Contribuir a una arquitectura de Internet de les coses per les ciutats que sigui oberta i interoperable.
  * Promoure projectes locals de cultura lliure que complementin o facin servir aquestes infraestructures.

**Experiències inspiradores**
  * TTNcat: https://thethingsnetwork.cat/
  * Experiències de mobilitat TTNcat: https://thethingsnetwork.cat/index.php/TTNcat_mobilitat  
  * Whitecat: https://whitecatboard.org/
  * SENTILO: http://www.sentilo.io/


## Desplegament de la xarxa d'accés de cinquena generació
**Objectius**
  * Impulsar que el desplegament de xarxes d'accés de cinquena generació (5G) es faci a través d'una xarxa compartida pels diferents proveïdors de serveis.

**Proposta programàtica**
  * Impulsar que el desplegament del servei d'accés 5G es faci a través d'una xarxa compartida pels diferents proveïdors de serveis.
  * Facilitar la implantació d'antenes per serveis de telecomunicació en edificis públics.

**Experiències inspiradores**
  * 5GCity: https://www.5gcity.eu/

## Contractació de serveis de telecomunicacions
**Objectius**
  * Potenciar les operadores de proximitat. Evitant el monopoli de les grans operadores.
  * Potenciar l'ús neutre de les xarxes de telecomunicacions.
  * Major control públic dels operadors.

**Proposta programàtica**
  * No contractar serveis a companyies de telecomunicacions que discriminin el trafic, el filtrin o l'interrompin. Un exemple de tràfic filtrat i blocat és el relacionat amb el referèndum d'autodeterminació de l'1 d'Octubre de 2017.
  * Preferència per proveïdors de serveis que estiguin connectats i intencanviin tràfic amb la resta de participants als punts d'intercanvi de tràfic locals (Catnix).

**Experiències inspiradores**
  * El nou contracte de serveis de telecomunicacions de l'Ajuntament de Barcelona incorpora mesures socials, laborals i ambientals: http://ajuntament.barcelona.cat/contractaciopublica/ca/noticia/el-nou-contracte-de-serveis-de-telecomunicacions-incorpora-mesures-socials-laborals-i-ambientals_432074


## Referències
  * Pacte Nacional per les Infrastructures: http://www.gencat.cat/especial/pni/cat/telecomunicacions.htm
  * Proposta d'Ordenança municipal per al desplegament de xarxes d'accés a serveis de telecomunicacions de nova generació i en format universal: https://fundacio.guifi.net/web/content/2322?unique=cef4bebe39b45ba50ed5ebb5e2a63ecaf07e6cb4&download=true
